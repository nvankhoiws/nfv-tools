{
	id: **Identifier**,
	notificationType: **String**,
	subscriptionId: **Identifier**,
	timeStamp: **DateTime**,
	nsdInfoId: **Identifier**,
	nsdId: **Identifier**,
	_links: {
		nsdInfo: **Link**,
		subscription: **Link**
	}
}