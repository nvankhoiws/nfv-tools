{
	id: **Identifier**,
	notificationType: **String **,
	subscriptionId: **Identifier**,
	timeStamp: **DateTime**,
	vnfInstanceId: **Identifier**,
	_links: **LccnLinks**
}