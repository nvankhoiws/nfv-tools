package example.apache.poi.sol;

import lombok.Data;
import org.apache.poi.xwpf.usermodel.*;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by KhoiNV6@vttek.vn on 1/14/19
 **/
@Data
public class Sol2 {
	public static final String DOCUMENT_PATH = "/home/vttek/nfv-tools/parse-doc-entity-etsi/docs/NFV-SOL002v245 clean.docx";
	public static final String OUTPUT_FOLDER = "/home/vttek/nfv-tools/parse-doc-entity-etsi/src/main/java/example/apache/poi/class_folder/";

	public static final String PACKAGE = "package example.apache.poi.class_folder;";
	public static String IMPORT_STATEMENT = ""
			+ "import lombok.Getter;" + "\n"
			+ "import lombok.Setter;" + "\n"
			+ "import lombok.AllArgsConstructor;" + "\n"
			+ "import lombok.NoArgsConstructor;" + "\n"
			+ "import lombok.ToString;" + "\n"
			+ "import lombok.Builder;" + "\n"
			+ "\n";
	public static final String CLASS_ETSI_NOTE =
			"/** " + "\n"
					+ "* Created by KhoiNV6@vttek.vn on " + new SimpleDateFormat("dd/MM/yyyy").format(new Date()) + "\n"
					+ "* This class is made by based on to ETSI GS NFV-SOL 003 v2.6.1 drafting version 2.5.4 (2018-12-19)" + "\n"
					+ "*/";
	public static final String ANNOTATED_STATEMENT = ""
			+ "@Getter" + "\n"
			+ "@Setter" + "\n"
			+ "@AllArgsConstructor" + "\n"
			+ "@NoArgsConstructor" + "\n"
			+ "@ToString" + "\n"
			+ "@Builder";

	private static List<String> listNameOfClasses = new ArrayList<>();
	private static List<XWPFTable> listTables = new ArrayList<>();
	private static List<String> listDescriptionTables = new ArrayList<>();
	private static Map<String,Object> masterMap = new HashMap<>();

	public static void main(String[] args) throws IOException {
		XWPFDocument document = getDocumentObject(DOCUMENT_PATH);
		masterMap = getInformation(document);
		listNameOfClasses = (List<String>)masterMap.get("classes");
		listDescriptionTables = (List<String>)masterMap.get("descriptions");
		listTables = (List<XWPFTable>)masterMap.get("tables");
		for (int i = 0; i < ((ArrayList) masterMap.get("tables")).size(); i++) {
			String textOfClass = composeClassText(listDescriptionTables.get(i), listNameOfClasses.get(i), listTables.get(i));
			saveClass(textOfClass, OUTPUT_FOLDER + listNameOfClasses.get(i) + ".java");
		}
	}

	private static void saveClass(String fulltext, String filename) throws FileNotFoundException {
		try (PrintWriter out = new PrintWriter(filename)) {
			out.println(fulltext);
		}
	}

	private static XWPFDocument getDocumentObject(String filename) throws IOException {
		FileInputStream in = new FileInputStream(new File(filename));
		return new XWPFDocument(in);
	}

	public static Map<String, Object> getInformation(XWPFDocument document) {
		List<XWPFTable> tableList = new ArrayList<>();
		List<String> classesNameList = new ArrayList<>();
		List<String> classDescriptionList = new ArrayList<>();
		Map<String, Object> map = new HashMap<>();

		List<IBodyElement> iter = document.getBodyElements();
		for (int i = 0; i < iter.size(); i++) {
			IBodyElement elem = iter.get(i);

			if (elem instanceof XWPFTable
					&& ((XWPFTable) elem).getRow(0).getTableCells().size() == 4
					&& ((XWPFTable) elem).getRow(0).getTableCells().get(0).getText().trim().toLowerCase().contains("Attribute name".toLowerCase())
					&& ((XWPFTable) elem).getRow(0).getTableCells().get(1).getText().trim().toLowerCase().contains("Data type".toLowerCase())
					&& ((XWPFTable) elem).getRow(0).getTableCells().get(2).getText().trim().toLowerCase().contains("Cardinality".toLowerCase())
					&& ((XWPFTable) elem).getRow(0).getTableCells().get(3).getText().trim().toLowerCase().contains("Description".toLowerCase())
					&& ((XWPFTable) elem).getRow(1).getTableCells().size() == 4
					&& ((XWPFParagraph) iter.get(i - 1)).getText().contains("Definition of the")
					&& ((XWPFParagraph) iter.get(i - 1)).getText().contains("data type")) {

				// add to table list
				tableList.add((XWPFTable) elem);

				// add to class name list
				String className = ((XWPFParagraph) iter.get(i - 1)).getText();
				className = className.substring(className.lastIndexOf(" the ") + 4, className.lastIndexOf(" data type")).trim();
				classesNameList.add(className);

				// get description of table
				String descriptionOfClass = "";

				int j = i;
				while (j >= 0) {
					IBodyElement prevElem = iter.get(j);
					if (prevElem instanceof XWPFParagraph
							&& ((XWPFParagraph) prevElem).getText().toLowerCase().contains(": " + className.toLowerCase())) {

						descriptionOfClass = ((XWPFParagraph) iter.get(j + 1)).getText() + "\n";

						int k = j + 2;
						while (!(((XWPFParagraph) iter.get(k)).getText().toLowerCase().contains("Definition of the ".toLowerCase())
								&& ((XWPFParagraph) iter.get(k)).getText().toLowerCase().contains(className.toLowerCase())
								&& ((XWPFParagraph) iter.get(k)).getText().contains("data type"))) {

							descriptionOfClass += "* " + ((XWPFParagraph) iter.get(k)).getText();
							++k;
						}
						break;
					}
					--j;
				}
				classDescriptionList.add(formatString(descriptionOfClass));

			}
		}
		map.put("tables", tableList);
		map.put("classes", classesNameList);
		map.put("descriptions", classDescriptionList);
		return map;
	}

	private static String formatString(String str) {
		int lengthCut = 110;
		String newStr = "";

		if (str.length() < lengthCut)
			return new String(str);

		int i = 1;
		while (i * lengthCut <= str.length()) {
			String smallCut = str.substring((i - 1) * lengthCut, i * lengthCut);
			newStr += smallCut+ "\n"
					+ "\t" + "* ";
			if ((i + 1) * lengthCut > str.length())
				newStr += str.substring(i * lengthCut, str.length());
			++i;
		}
		return newStr;
	}

	public static String composeClassText(String descriptionOfClass, String className, XWPFTable table) throws IOException {
		String fullTextOfClass = "";
		String classNote = "";
		String classNameText = "public class " + className;
		Boolean hasLastRow = false;

		int noOfRows = table.getRows().size();
		XWPFTableRow lastRow = table.getRow(noOfRows - 1);

		if (lastRow.getTableCells().size() == 1) {
			classNote += ""
					+ "/** " + "\n"
					+ "* Class Description:\n"
					+ "* " + descriptionOfClass + "\n"
					+ "* " + "\n"
					+ "* Class note:\n"
					+ "* " + formatString(lastRow.getCell(0).getText().trim()) + "\n"
					+ "*/";
			hasLastRow = true;
		} else {
			classNote += ""
					+ "/** " + "\n"
					+ "* Class Description:\n"
					+ "* " + descriptionOfClass + "\n"
					+ "*/";
		}

		if (hasLastRow) --noOfRows;
		boolean globalMulti = false;
		for (int i = 1; i < noOfRows; i++) {
			XWPFTableRow row = table.getRow(i);

			String attrName = row.getTableCells().get(0).getText().trim().replaceAll("\n", " ");
			String dataType = row.getTableCells().get(1).getText().trim().replaceAll("\n", " ");
			String cardinalityTxt = row.getTableCells().get(2).getText().trim().replaceAll("\n", " ");
			String descriptionTxt = formatString(row.getTableCells().get(3).getText());

			if (dataType.toLowerCase().contains("Not specified".toLowerCase())) {
				dataType = "Object";
			}
			if (dataType.toLowerCase().contains("Number".toLowerCase())) {
				dataType = "Integer";
			}
			if (dataType.toLowerCase().contains("KeyValuePair".toLowerCase())) {
				dataType = "KeyValuePairs";
			}
			if (dataType.toLowerCase().contains("Version".toLowerCase())) {
				dataType = "String";
			}
			if (dataType.toLowerCase().contains("Identifier".toLowerCase())) {
				dataType = "String";
			}

			fullTextOfClass +=
					"\t" + "/** " +"\n"
							+"\t" + "* " + cardinalityTxt + "\t" + dataType + "\t" +"\n"
							+ "\t" + "* " + descriptionTxt + "\n"
							+ "\t" + "**/" + "\n";

			Boolean multi = false;
			if (cardinalityTxt.trim().equalsIgnoreCase("2..N")
					|| cardinalityTxt.trim().equalsIgnoreCase("0..N")
					|| cardinalityTxt.trim().equalsIgnoreCase("1..N")) globalMulti = multi = true;

			fullTextOfClass += "\t" + "private " + (multi ? "List<" + dataType + ">" : dataType) + " " + attrName + ";\n\n";
		}
		if (globalMulti) IMPORT_STATEMENT += "import java.util.List;";

		fullTextOfClass = PACKAGE + "\n\n"
				+ IMPORT_STATEMENT + "\n\n"
				+ CLASS_ETSI_NOTE + "\n\n"
				+ classNote + "\n"
				+ ANNOTATED_STATEMENT + "\n"
				+ classNameText + " {\n"
				+ fullTextOfClass
				+ "}";
		return fullTextOfClass;
	}
}