@startuml
!include skin.inc

participant "OSS/BSS" as cli
participant "NFVO" as srv

cli -> srv: POST .../subscriptions (NsdmSubscriptionRequest)

opt
	note over srv
		test notification endpoint
	end note
	srv -> cli: GET <<Client side URI>> 
	cli -> srv: 204 No Content
end

srv -->>srv: Create subscription\n resource
srv -> cli: 201 Created (NsdmSubscription)


opt
	note over cli
		Client re-synchronizes all 
		or selected subscriptions 
		e.g. after an error
	end note

	cli -> srv: GET .../subscriptions/
	srv -> cli: 200 OK (NsdmSubscription[])

	cli -> srv: GET .../subscriptions/{subscriptionId}
	srv -> cli: 200 OK (NsdmSubscription)
end

note over cli
	Client does not need the 
	subscription anymore
end note

cli -> srv: DELETE .../subscriptions/{subscriptionId}
srv -> cli: 204 No Content

@enduml


