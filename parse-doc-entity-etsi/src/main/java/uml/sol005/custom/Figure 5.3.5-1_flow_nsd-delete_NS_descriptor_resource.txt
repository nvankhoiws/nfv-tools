@startuml
!include skin.inc

participant "OSS/BSS" as cli
participant "NFVO" as srv

note over cli, srv
Precondition: NSD has been on boarded to the NFVO,
the operational state of the NSD is DISABLED,
and the usage state of the NSD is NOT_IN_USE.
end note

cli -> srv: DELETE .../ns_descriptors/{nsdInfoId}
srv-->>srv: deletes an individual NS descriptor resource

srv -> cli: 204 No Content
srv -->> cli: <i>Send NsdDeletionNotification to OSS/BSS</i>

note over cli, srv
	Postcondition: The individual NS descriptor resource is deleted.
end note
@enduml