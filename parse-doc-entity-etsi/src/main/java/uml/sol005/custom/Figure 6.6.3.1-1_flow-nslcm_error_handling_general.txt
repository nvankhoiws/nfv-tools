
@startuml
!include skin.inc

'''--------- Immediate operation failure

participant "OSS/BSS" as cli
participant NFVO as srv

cli -> srv: Initiate NS LCM operation over HTTP

note over srv
static precondition check fails
or other failure occurs
end note

srv -> cli: HTTP error code specific to failure + Error description in body

note over srv
end note

@enduml