
@startuml
!include skin.inc

'''--------- Operation error resolution options

participant "OSS/BSS" as cli	
participant NFVO as srv

cli -> srv: Initiate operation over HTTP
srv -> cli: Accepted with NS LCM operation occurrence id

srv -->> cli: Send NsLcmOperationOccurrenceNotification(start)


note over srv
	NFVO starts operation-specific logic. The operation 
	and possible automatic retries fail, which is reported 
	as a temporary failure.
end note

srv -->> cli: Send NsLcmOperationOccurrenceNotification(result, FAILED_TEMP)
...

loop until success or (manually) determining permanent failure
	alt Retry (if supported for the operation)

		note over cli,srv
			(Manual) error investigation
			If applicable, investigate and possibly resolve
			the root cause of the failure (manually)
		end note

		cli -> srv: Initiate operation retry for given NS LCM operation occurrence id
		srv -> cli: OK  

		alt retry failure
			srv -->> cli: Send NsLcmOperationOccurrenceNotification(result, FAILED_TEMP, changes)

			note over cli
				Optional: manually investigate cause of retry failure 
				and resolve errors, determine whether to start retry again.
			end note
		else retry success
			srv -->> cli: Send NsLcmOperationOccurrenceNotification(result, COMPLETED, changes)
		end

	else Rollback (if supported for the operation)

		cli -> srv: Initiate operation rollback for given NS LCM operation occurrence id
		srv -> cli: OK

		alt rollback failure
			srv -->> cli: Send NsLcmOperationOccurrenceNotification(result, FAILED_TEMP, changes)

			note over cli
				Optional: manually investigate cause of rollback failure 
				and resolve errors, determine whether to start rollback again.
			end note

		else rollback success
			srv -->> cli: Send NsLcmOperationOccurrenceNotification(result, ROLLED_BACK, changes)
		end

	else Fail 
		note over cli
			Operator or OSS/BSS determines that it makes 
			no sense to start retry or rollback again
		end note

		cli -> srv: Fail operation for given NS LCM operation occurrence id
		srv -> cli: OK 
		srv -->> cli: Send NsLcmOperationOccurrenceNotification(result, FAILED, changes)
	end
end loop
@enduml
