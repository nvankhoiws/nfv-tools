@startuml
!include skin.inc
participant "OSS/BSS" as cons
participant "NFVO" as prod 

cons -> prod: POST .../subscriptions (FmSubscriptionRequest)

opt 
	note over prod
		test notification endpoint
	end note
	prod -> cons: GET <<Client side URI>>
	cons -> prod: 204 No Content
end
prod -> prod: Create subscription resource
prod -> cons: 201 Created (FmSubscription)

opt
	note over cons
		Client re-synchronizes all 
		or selected subscriptions 
		e.g. after an error
	end note
	cons -> prod: GET .../subscriptions/
	prod -> cons: 200 OK (FmSubscription[])
	cons -> prod: GET .../subscriptions/{subscriptionId}
	prod -> cons: 200 OK (FmSubscription)
end

note over cons
	Client does not need the subscription anymore
end note

cons -> prod: DELETE .../subscriptions/{subscriptionId}
prod -> cons: 204 No Content
@enduml