@startuml
!include skin.inc

participant "OSS/BSS" as cli
participant "NFVO" as srv

note over cli,srv
	Precondition: OSS/BSS has subscribed previously
end note	

note over srv
	Event occurs that 
	matches subscription
end note

srv -> cli: POST <<Client side URL>> (<<Notification>>)
cli -> srv: 204 No Content

@enduml