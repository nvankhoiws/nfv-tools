@startuml

!include skin.inc
participant "OSS/BSS" as cli
participant "NFVO" as srv

note over cli, srv
	Precondition: The VNF package is on-boarded to the NFVO.
end note

alt fetch the whole content of the artifact
	cli -> srv: GET .../vnf_packages/{vnfPkgId}/artifacts/{artifactPath}
	srv -> cli: 200 OK (artifact file)
else fetch the artifact using partial download
	cli -> srv: GET .../vnf_packages/{vnfPkgId}/artifacts/{artifactPath} (Range: bytes=xxx-yyy)
	srv -> cli: 206 Partial Content (Content-Range: bytes=xxx-yyy/zzz, Partial content of the artifact file)
end	

@enduml 
