@startuml
!include skin.inc
participant "OSS/BSS" as cons
participant "NFVO" as prod 

	
alt query information about multiple alarms
	cons -> prod: GET .../alarms
        prod -> cons: 200 OK (Alarm[]) 
else read information about individual alarm
	cons -> prod: GET .../alarms/{alarmId}
        prod->cons: 200 OK (Alarm)
end


@enduml

