@startuml
!include skin.inc
participant "OSS/BSS" as cons
participant "NFVO" as prod 

note over cons, prod
	Precondition: OSS/BSS has subscribed previously
end note 
note over prod
	Event occurs that
	matches subscription
end note 
prod -> cons: POST <<Client side URI>> (<<Notification>>)
cons -> prod: 204 No Content 

@enduml

